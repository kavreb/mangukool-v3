﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mangukool_v3;

namespace KooliAken
{
    public partial class Form1 : Form
    {
        Human who = null;

        static List<Form1> Windows = new List<Form1>();
        public Form1()
        {
            InitializeComponent();
            Windows.Add(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.button1.Text == "Log On")
            {
                who = Human.ByIK(textBox1.Text);
                if (who == null)
                {
                    this.label2.Text = "Ma ei tunne sind. Proovi uuesti.";
                }

                else
                { 
                    this.button2.Visible = true;
                    this.label2.Text = "";
                    this.textBox1.Text = "";
                    this.button1.Text = "Log Off";
                    
                    if (who is Student whoS)
                    {
                        this.label1.Text = $"Tere õpilane {whoS.Name} (klassist {whoS.StudentClass}).";
                    }

                    else if (who is Grownup whoG)
                    {
                        this.label1.Text = "Tere ";
                        if (whoG.IsParent)
                        {
                            if (whoG.IsTeacher)
                                this.label1.Text += "lapsevanem, ";
                            else
                                this.label1.Text += "lapsevanem ";
                        }

                        if (whoG.IsTeacher)
                        {
                            
                                this.label1.Text += "õpetaja ";

                            if (whoG.IsLeader)
                            {
                                this.label1.Text +=  $"ja {whoG.Klass} klassijuhataja ";
                            }

                        }

                        this.label1.Text += $"{whoG.Name}.";
                    }

                }
                
            }
            else if (this.button1.Text == "Log Off")
            {
                this.label1.Text = "";
                this.label2.Text = "Palun logi sisse";
                this.button1.Text = "Log On";
                this.button2.Visible = false;
                                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (who is Student whoS)
                (new Form2() { Who = whoS }).Show();
            else
                (new Form3() { Who = (Grownup)who }).Show();
            //eelnevas on näitena kaks versiooni, kuidas defineerida muutuja, mille väärtuse omistame erinevate Form'ide Who'dele
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
