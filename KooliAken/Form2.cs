﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mangukool_v3;

namespace KooliAken
{
    public partial class Form2 : Form
    {
        internal Student Who;

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.label1.Text = Who.ToString();

        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<Student> Classmates = new List<Student>();

            foreach (var x in Student.Students)
            {
                if (x.ID != Who.ID)
                    if (x.StudentClass == Who.StudentClass)
                        {
                            Classmates.Add(x);
                        }
            }

            this.dataGridView1.DataSource = Classmates;
            dataGridView1.Columns[1].Visible = false;
            dataGridView1.Columns[0].HeaderText = "Class";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<Subject> AllMySubjects = new List<Subject>();

            foreach (var classo in Level.Levels)
            {
                if (classo.Key == Who.StudentClass)
                {
                    foreach (var item in classo.Value.GetClassSubjects())
                    {
                        AllMySubjects.Add(Subject.FindSubject(item));
                    }
                    
                }
            }
           
            this.dataGridView1.DataSource = AllMySubjects;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Subject";
        }


        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
