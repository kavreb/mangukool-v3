﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Mangukool_v3
{
    public enum Grade { Nõrk = 1, Rahuldav, Hea, Väga_hea, Suurepärane }

    public class SubjectGrade
    {
        static string Filename = "grades";
        public static List<SubjectGrade> Grades = new List<SubjectGrade>();
        private string _ID; public string ID => _ID;
        private string _Aine; public string Aine => _Aine;
        private string _Grade;
        string _tID; public string tID => _tID; //teacher ID
        public DateTime When = DateTime.Now.Date;

        static SubjectGrade()
        {
            ReadFile();
        }

        private SubjectGrade(string id, string subject, string grade)
        {
            _ID = id;
            _Aine = subject;
            _Grade = grade;

            //tID = givenCode ;
            When = DateTime.Now.Date;

            Grades.Add(this);
        }

        public string Grade
        {
            get => _Grade;
            private set
            {
                _Grade = value;
            }
        }

        static void ReadFile()
        {
            string[] readLines = Filename.ReadFile();
            foreach (var line in readLines)
            {
                string[] parts = line.Split(',');
                string id = parts[0].Trim();
                string subject = parts[1].Trim();
                string grade = parts[2].Trim();
                new SubjectGrade(id, subject, grade);
            }
        }

        static void WriteFile()
        {
            File.WriteAllLines(Filename,
                Grades.Select(x => $"{x.ID},{x.Aine},{x.Grade}"));
        }

        public static void AddGrade(string id, string aine, string hinne)
        {
            if (Human.Humans.Keys.Contains(id))
            {
                new SubjectGrade(id, aine, hinne);
                WriteFile();
            }
            else Console.WriteLine("vale kood, anna uus");
        }

        public override string ToString()
        {
            return $"Õpilase {Human.ByIK(ID)?.Name} hinne aines {Subject.FindSubject(Aine)} on {Grade}";
        }

    }
}

