﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mangukool_v3
{
    public class Grownup : Human
    {
        static string Filename = "grownups";

        Grownup(string id, string nimi) : base(id, nimi) { }

        public static IEnumerable<Grownup> Grownups => Human.Nimekiri.OfType<Grownup>();
        public static IEnumerable<Grownup> Teachers => Human.Nimekiri.OfType<Grownup>().Where(x => x.Aine != "");
        public static IEnumerable<Grownup> Parents => Human.Nimekiri.OfType<Grownup>().Where(x => x.IsParent);
        public static Dictionary<string, string> TeachersList => Human.Nimekiri.OfType<Grownup>().Where(x => x.IsTeacher).ToDictionary(x => x.ID, x => x.Name);
        public static Dictionary<string, string> ParentsList => Human.Nimekiri.OfType<Grownup>().Where(x => x.IsParent).ToDictionary(x => x.ID, x => x.Name);
        
        string _Aine = ""; public string Aine => _Aine;
        public bool IsTeacher => _Aine != "";

        string _Klass = ""; public string Klass => _Klass;
        public bool IsLeader => _Klass != "";

        List<Student> _Kids = new List<Student>();
        IEnumerable<Student> Kids => _Kids.AsEnumerable();
        public bool IsParent => _Kids.Count > 0;
        

        static Grownup()
        {
            ReadFile();
        }

        static public void ReadFile()
        {

                Filename.ReadFile()
                    .Select(x => (x + ",,,,").Split(',').Select(y => y.Trim()).ToArray())
                    .Select(x => new { Human = new Grownup(x[0], x[1]), Muud = x.Skip(2).ToArray() })
                    .ToList()
                    .ForEach(
                            x =>
                            {
                                x.Human._Aine = x.Muud[0];
                                x.Human._Klass = x.Muud[1];
                                x.Human._Kids = x.Muud.Skip(2).Where(y => y != "").Select(y => Human.ByIK(y)).OfType<Student>().ToList();
                            }
                            );

            //    string[] loetudRead = Utility.Readfile(Filename);
            //    foreach (var loetudrida in loetudRead)
            //    {
            //        string[] osad = (loetudrida + ",,,,").Split(',');
            //        var grown = new Grownup(osad[0].Trim(), osad[1].Trim());
            //        grown._Aine = osad[2].Trim();
            //        grown._Klass = osad[3].Trim();
            //        for (int i = 4; i < osad.Length; i++)
            //        {
            //            if(osad[i] != "")
            //            {
            //                Student laps = Human.ByIK(osad[i].Trim()) as Student; ;
            //                if (laps != null) grown._Kids.Add((Student)laps);
            //            }
            //        }
            //    }
        }

        public override string ToString()
        =>
            (IsTeacher ? 
                ((Subject.FindSubject(_Aine)?.GetsetName ?? _Aine) + " õpetaja"
                +
                    (IsLeader ? $" ja {Klass} klassi juhataja " : " ")
                ) : "") 
            + Name;
        

    }
}
