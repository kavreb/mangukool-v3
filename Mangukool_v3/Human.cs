﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mangukool_v3
{
    public class Human
    {
        public static Dictionary<string, Human> Humans
            = new Dictionary<string, Human>()
            ;

        public static IEnumerable<Human> Nimekiri => Humans.Values;  //why this? et saada public array, mida saab kasutada nii erinevatel viisidel?

        static Human()
        {
            Student.LoeFailist();
            Grownup.ReadFile();
        }
        private string _ID; public string ID => _ID;
        private string _Name; public string Name => _Name;
            //private string _Klass; public string Klass => _Klass;

        public static Human ByIK(string id) => Humans.Keys.Contains(id) ? Humans[id] : null;

        public Human(string id, string nimi, string klass = "")
        {
            _ID = id;
            _Name = nimi;
            //_Klass = klass;

            //try
            //{
            //    Humans.Add(id, this);
            //}
            //catch
            //{
            //    throw new Exception($"isikukood {id} on topelt");
            //}

            if (!Humans.Keys.Contains(id))
                Humans.Add(id, this);
        }
    }
}
