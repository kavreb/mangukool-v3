﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mangukool_v3
{
    public class Level
    {
        static string Filename = "levels";

        public static Dictionary<string, Level> Levels = new Dictionary<string, Level>();
                
        string _Klass; public string Klass => _Klass;

        List<string> _ClassSubjects = new List<string>();
        public List<string> GetClassSubjects()
        {
            return this._ClassSubjects;
        }
        
        public IEnumerable<Student> Students
            => Student.Students.Where(x => x.StudentClass == this._Klass);

        public Level(string klass)
        {
            _Klass = klass;
            Levels.Add(klass, this);
        }

        static Level() => ReadFromFile();

        static public void ReadFromFile()
        {
                Filename.ReadFile()
                  .Select( x => x.Split(',').Select(y => y.Trim()).ToList() )
                  .ToList()
                  .ForEach(x => new Level(x[0])._ClassSubjects = x.Skip(1).ToList());
        }

        /*
         Alternatiiv:
              static void LoeFailist()
        {
            string[] loetudRead = File.ReadAllLines(Filename);
            foreach(var loetudRida in loetudRead)
            {
                string[] jupid = loetudRida.Split(',');
                // jupid[0] on klassi kood-nimi "1A"
                // jupid[1..ülejäänud] on õppeaine koodid
                Klass õ = new Klass(jupid[0].Trim());
                for (int i = 1; i < jupid.Length; i++) õ.Ained.Add(jupid[i].Trim());
            }
        }

         */

        static void WriteFile()
        {
            File.WriteAllLines(Filename,
                Levels.Values
                .Select(x => x.Klass + string.Join("", x._ClassSubjects.Select(y => $", {y}")))
                );
        }

        public static Level ByCode(string klass)
            => Levels.Keys.Contains(klass) ? Levels[klass] : null;

        //ClassStudents pmst koostab dictionary, kus võtmesõnaks on klass ning väärtuseks on nimekiri klassis olevate õpilastega
        static public Dictionary<string, List<string>> ClassStudents => Student.Students
                                                                            .ToLookup(x => x.StudentClass)
                                                                            .ToDictionary(x => x.Key, x => x.Select(y => y.Name)
                                                                            .ToList());

        //static public List<Subject> ClassSubjects(Level nimi) => Subject.Subjects
        //                                                                .Select(x => x.Value.Kood.Contains(nimi.Klass))
        //                                                                .ToArray()
        //                                                                .ToList();


        
        public override string ToString()
        {
            foreach (var x in _ClassSubjects)
            {
                return x;
            }

            return "";
            //return $"Klassis {Klass} on ained {string.Join (", ", Ained)}";
        }
    }
}
