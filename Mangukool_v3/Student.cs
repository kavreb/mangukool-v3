﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mangukool_v3
{
    public class Student : Human
    {
        private static string Filename = "students";

        public static IEnumerable<Student> Students
            => Human.Nimekiri.OfType<Student>();

        private string _Klass; public string StudentClass => _Klass;

        public Student(string id, string nimi) : base(id, nimi) { }

        static Student() => LoeFailist();

        static public void LoeFailist()
        {
            Filename.ReadFile()
            .Select(x => x.Split(',').Select(y => y.Trim()).ToArray())
            .ToList()
            .ForEach(x => new Student(x[0], x[1])._Klass = x[2]);
                                 
        }        

        public void WriteFile()
        {
            Utility.WriteFile(Students.Select(x => $"{x.ID}, {x.Name}, {x.StudentClass}"), Filename);

            //File.WriteAllLines(Filename,
            //    Students.Select(x => $"{x.ID}, {x.Name}, {x.Klass}"));
        }


        public override string ToString()
        {
            return $"{StudentClass} klassi õpilane {Name} (ID {ID})";
        }
    }
}
