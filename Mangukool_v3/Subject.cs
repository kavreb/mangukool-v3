﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Mangukool_v3
{
    public class Subject
    {
        private static string Filename = "subjects";
        static public Dictionary<string, Subject> Subjects = new Dictionary<string, Subject>();
        private string _Code; public string GetsetCode => _Code;
        private string _Name;
        public string GetsetName
        { 
            get => _Name;
            set
            {
                if (value != "")
                {
                    if (value != _Name)
                    {
                        _Name = value;
                        WriteToFile();
                    }
                }
            }
        }

        static Subject()
        {
            ReadFromFile();
        }

        private Subject(string code, string name)
        {
            _Code = code;
            _Name = name;
            Subjects.Add(code, this);
        }

        private static void ReadFromFile()
        {
            string[] readLines = Filename.ReadFile();
            foreach (var rida in readLines)
            {
                string[] parts = rida.Split(',');
                string code = parts[0].Trim();
                string name = parts[1].Trim();
                if (!Subject.Subjects.Keys.Contains(code))
                {
                    new Subject(code, name);
                }
            }
        }

        private static void WriteToFile()
        {
            List<string> writtenLines = new List<string>();
            foreach (var x in Subject.Subjects.Values)
            {
                string writtenLine = $"{x._Code}, {x._Name}";
                writtenLines.Add(writtenLine);
            }
            Utility.WriteFile(writtenLines, Filename);
            
        }

        public static bool AddSubject(string kood, string nimetus)
        {
            if (Subjects.Keys.Contains(kood)) return false;
            new Subject(kood, nimetus);
            WriteToFile();
            return true;
        }

        public static bool DeleteSubject(string kood)
        {
            if(Subjects.Keys.Contains(kood))
            {
                Subjects.Remove(kood);
                WriteToFile();
                return true;
            }
            else return false;
        }

        public static Subject FindSubject(string code)
        {
            if (Subjects.Keys.Contains(code)) return Subjects[code];
            else return null;
        }

        public override string ToString()
        {
            return $"{GetsetName}";
        }
    }
}
